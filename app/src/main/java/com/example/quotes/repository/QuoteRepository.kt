package com.example.quotes.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.example.quotes.paging.QuotePagingSource
import com.example.quotes.retrofit.QuotesApi
import javax.inject.Inject

class QuoteRepository @Inject constructor(val quotesApi: QuotesApi) {

    fun getQuotes() = Pager(
        config = PagingConfig(pageSize = 20, maxSize = 100),
        pagingSourceFactory = { QuotePagingSource(quotesApi) }
        ).liveData


}