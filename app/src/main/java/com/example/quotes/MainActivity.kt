package com.example.quotes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quotes.paging.PagingQuoteAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: PagingQuoteAdapter
    private lateinit var viewModel : QuoteViewHolder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("TAG", "onCreate: ")


        recyclerView = findViewById(R.id.quotes_list)
        adapter = PagingQuoteAdapter()
        viewModel = ViewModelProvider(this)[QuoteViewHolder::class.java]
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter


        viewModel.list.observe(this, Observer {
            Log.d("QQQQQ", "onCreate: $it")
            adapter.submitData(lifecycle, it)
        })

//        viewModel.list.observe(this) { pagingData ->
//
//            // Now, you can work with the list of items
//            Log.d("QQQQQ", "onCreate: $pagingData")
//
//            adapter.submitData(lifecycle, pagingData)
//
//        }


    }



}