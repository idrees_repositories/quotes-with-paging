package com.example.quotes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.quotes.repository.QuoteRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class QuoteViewHolder @Inject constructor(private val repository: QuoteRepository) : ViewModel() {


    val list = repository.getQuotes().cachedIn(viewModelScope)


}