package com.example.quotes.di

import com.example.quotes.retrofit.QuotesApi
import com.example.quotes.utils.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {



    @Singleton
    @Provides
    fun getRetrofit() : Retrofit{
            return Retrofit.Builder().baseUrl(BASE_URL).
            addConverterFactory(GsonConverterFactory.create()).build()
    }


    @Singleton
    @Provides
    fun getQuoteApi(retrofit: Retrofit) : QuotesApi{
        return retrofit.create(QuotesApi::class.java)
    }



}